package system

type ServiceGroup struct {
	JwtService
	OperationLogService
	MenuService
	UserService
	RoleService
	MenuButtonService
	ButtonService
	DeptService
	SystemConfigService
}
