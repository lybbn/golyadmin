/**
 * @Description 系统自带路由
 **/
package system

type RouterGroup struct {
	BaseRouter
	UserRouter
	OperationLogRouter
	MenuRouter
	MenuButtonRouter
	ButtonRouter
	DeptRouter
	RoleRouter
	FileRouter
	SystemRouter
}
