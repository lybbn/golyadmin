# golyadmin web 前端框架

## slogon

做一个专业前端能用的框架，后台人员也能面向配置的、能改得动的CRUD

## 介绍

golyadmin 是一套前后端分离的前端后台管理框架，是适配 golyadmin 的 go语言 后台管理项目的专属框架，基于原生 vue3 elmentuiplus 开发，灵活自定义，可发挥空间大

## 软件架构


```
1、vue3
2、elementplus
3、pinia
4、富文本编辑器采用tinymce
```

## 安装教程


```
cd web
npm install --registry=https://registry.npm.taobao.org

```


## 使用说明

调试开发直接运行(development)： 

```
npm start
```

默认监听端口：8090

## 打包

线上部署(production)： 

```
npm run build
```

打包后静态文件在 dist 目录中

## 商用注意事项

如果您将此项目用于商业用途，请遵守Apache2.0协议并保留作者文件头部等信息声明。
